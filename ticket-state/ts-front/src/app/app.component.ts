import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { MatButton, MatIconButton } from '@angular/material/button';
import {
  MatSidenav,
  MatSidenavContainer,
  MatSidenavContent,
} from '@angular/material/sidenav';
import { MatToolbar } from '@angular/material/toolbar';
import { MatIcon } from '@angular/material/icon';
import SidenavMenuComponent from './sidenav-menu/sidenav-menu.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    MatToolbar,
    MatSidenav,
    MatSidenavContainer,
    MatSidenavContent,
    MatIcon,
    MatButton,
    MatIconButton,
    SidenavMenuComponent,
  ],
  templateUrl: 'app.component.html',
  styleUrl: 'app.component.scss',
})
export default class AppComponent {
  public title = 'ts-front';
}
