import { Component, Input } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatSidenav } from '@angular/material/sidenav';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-sidenav-menu',
  templateUrl: 'sidenav-menu.template.html',
  standalone: true,
  imports: [MatButton, RouterLink],
})
export default class SidenavMenuComponent {
  // public sidenav: Signal<MatSidenav> = input();
  @Input({ required: true }) public sidenav!: MatSidenav;
}
