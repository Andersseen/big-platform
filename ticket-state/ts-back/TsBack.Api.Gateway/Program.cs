using Microsoft.AspNetCore.Authentication.BearerToken;
using System.Security.Claims;

var builder = WebApplication.CreateBuilder(args);

var conf = builder.Configuration.GetSection(key:"ReverseProxy");    
builder.Services.AddReverseProxy().LoadFromConfig(conf);
builder.Services.AddAuthentication(BearerTokenDefaults.AuthenticationScheme)
    .AddBearerToken();

builder.WebHost.ConfigureKestrel(options =>
{
    options.ListenAnyIP(8000);
});

builder.Services
    .AddAuthorization(o => 
    {
        o.AddPolicy("first-api-access", 
                    policy => policy
                                .RequireAuthenticatedUser()
                                .RequireClaim("first-api-access", true.ToString()));
        o.AddPolicy("second-api-access",
                   policy => policy
                               .RequireAuthenticatedUser()
                               .RequireClaim("second-api-access", true.ToString()));
    });
// Add services to the container.
var app = builder.Build();

app.MapGet("login", (bool firstApi = false, bool secondApi = false) => 
Results.SignIn(
    new ClaimsPrincipal(
        new ClaimsIdentity(
            [
                new Claim("sub", Guid.NewGuid().ToString()),
                new Claim("first-api-access", firstApi.ToString()), 
                new Claim("second-api-access", firstApi.ToString()), 
            ],
             BearerTokenDefaults.AuthenticationScheme)),
        authenticationScheme: BearerTokenDefaults.AuthenticationScheme));

// Configure the HTTP request pipeline
app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.MapReverseProxy();
app.Run();
