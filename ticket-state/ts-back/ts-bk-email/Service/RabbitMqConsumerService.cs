﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading.Channels;

namespace ts_bk_email.Service
{
    public class RabbitMqConsumerService : BackgroundService
    {
      
            private IConnection _connection;
        private IModel _channel;
        private const string HostName = "rabbitmq"; // Cambia esto si tu RabbitMQ no está en localhost
        private const int Port = 5672;
        private const string UserName = "guest";
        private const string Password = "guest";
        private const string QueueName = "email";

        public RabbitMqConsumerService()
        {
            var factory = new ConnectionFactory()
            {
                HostName = HostName,
                Port = Port,
                UserName = UserName,
                Password = Password
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: QueueName,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                Console.WriteLine($"Received message: {message}");

                
            };

            _channel.BasicConsume(queue: QueueName,
                                 autoAck: true,
                                 consumer: consumer);

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }
    }
}
