
from sqlite3 import Date
import pika

def callback(ch, method, properties, body):
    print(f" [x] Received {body}")+(Date.day)

def consume_message():
    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq'))
    channel = connection.channel()
    channel.queue_declare(queue='email')

    channel.basic_consume(queue='email',
                          on_message_callback=callback,
                          auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    consume_message()