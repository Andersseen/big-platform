﻿using Microsoft.EntityFrameworkCore;
using ts_bk_ticket.Models;

namespace ts_bk_ticket.Data
{
    public class TicketDBContext : DbContext
    {
        public TicketDBContext(DbContextOptions<TicketDBContext> options): base(options)
        {
            
        }

        public DbSet<Ticket> Tickets => Set<Ticket>();
    }
}
