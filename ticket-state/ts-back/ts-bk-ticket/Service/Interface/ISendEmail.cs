﻿namespace ts_bk_ticket.Service.Interface
{
    public interface ISendEmail
    {
        public bool Send(string email, string message);
    }
}
