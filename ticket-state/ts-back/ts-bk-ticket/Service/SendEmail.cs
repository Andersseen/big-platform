﻿using RabbitMQ.Client;
using System.Text;
using ts_bk_ticket.Service.Interface;

namespace ts_bk_ticket.Service
{

    public static class Rabbit
    {
        public static void EnviarRabbit()
        {
            var factory = new ConnectionFactory() { HostName = "rabbitmq" };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "email",
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    var body = Encoding.UTF8.GetBytes("enviando mensajito desde ticket un email");

                    channel.BasicPublish(exchange: "",
                                         routingKey: "email",
                                         basicProperties: null, body: body);

                    Console.WriteLine("Menssaje enviado por MQ Rabbit");

                }
            }
        }
    }
    public class SendEmail : ISendEmail
    {
        public bool Send(string email, string message)
        {
            var factory = new ConnectionFactory() { HostName = "127.0.0.1"};
            using (var connection = factory.CreateConnection()) 
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "email", durable:false, exclusive:false, autoDelete:false, arguments:null);

                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "", routingKey: "hello", basicProperties: null, body: body);

                    Console.WriteLine("Menssaje enviado por MQ Rabbit");
                    return true;
                }
            }
        }
    }
}
