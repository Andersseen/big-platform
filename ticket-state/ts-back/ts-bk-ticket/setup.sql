-- create the database
CREATE DATABASE ts_postgres;

--INSERT INTO "Tickets" ("Title", "Description", "Role", "State", "Create_user", "Update_user", "Update_date", "Created_date")
--VALUES 
--('Create navigation bar', 'Develop a responsive navigation bar using CSS Flexbox and JavaScript', 1, 1, 1, 1, 2, 20230901),
--('Build landing page', 'Design and implement the landing page using HTML, CSS, and animations', 2, 1, 1, 1, 2, 20230902),
--('Integrate REST API', 'Fetch and display data from a REST API using Axios in React', 3, 2, 3, 1, 4, 20230903),
--('Fix CSS layout issues', 'Resolve issues with grid and flexbox layouts across different screen sizes', 4, 1, 1, 1, 3, 20230904),
--('Optimize image loading', 'Use lazy loading techniques to improve image load times', 2, 3, 5, 1, 6, 20230905),
--('Refactor React components', 'Refactor class-based components to functional components with hooks', 1, 2, 2, 1, 1, 20230906),
--('Implement form validation', 'Add client-side validation for forms using JavaScript and Regex', 3, 3, 3, 2, 4, 20230907),
--('Create modal component', 'Develop a reusable modal component in React for confirmation dialogs', 4, 1, 1, 3, 2, 20230908),
--('Add dark mode', 'Implement dark mode toggle functionality using CSS variables and JavaScript', 1, 2, 2, 4, 3, 20230909),
--('Deploy frontend app', 'Deploy the frontend application to a hosting service like Netlify or Vercel', 2, 3, 3, 4, 5, 20230910);