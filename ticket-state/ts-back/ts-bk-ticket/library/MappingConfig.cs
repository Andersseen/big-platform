﻿using AutoMapper;
using ts_bk_ticket.Models;
using ts_bk_ticket.Models.DTO;

namespace ts_bk_ticket.library
{
    public class MappingConfig
    {
        public static MapperConfiguration RegisterMaps()
        {
            var mappingConfig = new MapperConfiguration(config => {
                config.CreateMap<TicketDTO, Ticket>();
                config.CreateMap<Ticket, TicketDTO>();
                config.CreateMap<TicketUpdateDTO, Ticket>();
            });
            return mappingConfig;
        }
    }
}
