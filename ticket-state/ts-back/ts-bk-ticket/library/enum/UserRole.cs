﻿namespace ts_bk_ticket.library.@enum
{
    public enum UserRole
    {
        Client = 0,
        Admin = 1
    }
}
