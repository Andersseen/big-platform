﻿namespace ts_bk_ticket.library.@enum
{
    public enum TicketState
    {
        AnalyzeRequest = 0,
        Analyzing = 1,
        DevelopmentRequest = 2,
        Development = 3,
        Done = 4
    }
}
