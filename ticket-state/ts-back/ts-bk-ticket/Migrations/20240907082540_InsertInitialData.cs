﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ts_bk_ticket.Migrations
{
    /// <inheritdoc />
    public partial class InsertInitialData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // Ruta del archivo SQL
        var sqlFile = Path.Combine("Migrations/script", "insertTickets.sql");

        // Leer el contenido del archivo SQL
        var sqlScript = File.ReadAllText(sqlFile);

        // Ejecutar el archivo SQL
        migrationBuilder.Sql(sqlScript);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
