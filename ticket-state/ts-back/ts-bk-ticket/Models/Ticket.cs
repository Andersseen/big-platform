﻿using ts_bk_ticket.library.@enum;


namespace ts_bk_ticket.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public string Title { get; set; } = "";
        public string Description { get; set; } = "";
        public  UserRole Role { get; set; }
        public TicketState State { get; set; }
        public int Create_user { get; set; }
        public int Update_user { get; set;}
        public  int Created_date { get; set; }
        public int Update_date { get; set;}
    }
}
