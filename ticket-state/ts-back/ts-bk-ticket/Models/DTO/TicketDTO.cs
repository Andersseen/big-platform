﻿using ts_bk_ticket.library.@enum;

namespace ts_bk_ticket.Models.DTO
{
    public class TicketDTO
    {
        public string Title { get; set; } = "";
        public string Description { get; set; } = "";
        public UserRole Role { get; set; }
        public TicketState State { get; set; }
    }
}
